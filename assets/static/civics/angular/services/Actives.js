angular.module('civics.actives_service', [])

.factory('actives_data', function($cacheFactory){
    return $cacheFactory('actives');
})

.factory('Actives', function($http, Settings, Categories, $rootScope, $q, meta, actives_data)
{
    var actives = {};

    actives.createCategories = function(){
        $http.get('/api/cities', {
            ignoreLoadingBar: true,
        }).then(
            function(response){
                for(var i in response.data){
                  var city = response.data[i];
                  /** Update initiative cities category for the filters */
                  Categories.addCity('España', city.name, city.id, city.coordinates);
                }
            }
        );
    };

    // Returns a list of events from cached data
    actives.createList = function(){
        var items = [];
        var actives = actives_data.get('actives');
        actives.forEach( function(active){
            var categories = [ active.category, ...active.category_alt ];
            items.push({
                  name           : active.name,
                  pk             : active.pk,
                  category       : active.category,
                  category_names : categories.map( function(i){ return Categories.categories[ i ] } ),
                  categories     : categories, 
                  ods            : active.ods,
                  ods_name       : Categories.ods[ active.ods ],
                  agents         : active.agent,
                  agent_name     : Categories.agents[ active.agent ],
                  cities         : active.city,
                  img            : active.image,
            });
        } );
        meta.count = items.length;
        return items;
    };

    // Returns a list of clusters from cached data
    actives.createClusters = function(){
        PruneCluster.Cluster.ENABLE_MARKERS_LIST = true;
        meta.count = 0;
        var clusters = {};
        var actives = actives_data.get('actives');
        actives.forEach( function(marker){
            var city = marker.city;
            if(!(city in clusters)){
                clusters[city] = new PruneClusterForLeaflet();
            }
            var pos = JSON.parse(marker.position);
            var marker_categories = [ marker.category, ...marker.category_alt ];
            var m = new PruneCluster.Marker(pos.coordinates[1], pos.coordinates[0], {
                id            : marker.pk,
                cities        : city,
                category_main : marker.category,
                categories    : marker_categories,
                ods           : marker.ods, 
                agents        : marker.agent,
                relevance     : marker.relevance
            });
            clusters[city].RegisterMarker(m);
            meta.count++;
        });
        var sizes = [ [], [30, 45], [40, 60], [50, 75] ];
        for(city in clusters){
            clusters[city].PrepareLeafletMarker = function(leafletMarker, data){
                leafletMarker.setIcon( L.divIcon({
                    'iconSize'    : sizes[data.relevance],
                    'iconAnchor'  : [sizes[data.relevance][0]*.5, sizes[data.relevance][1]],
                    'className'   : 'cm cm--' + data.id + ' size--' + data.relevance,
                    'html'        : "<i class='outer" + " i-ods-" + data.ods + " i-ag-" + data.agents + "'></i>" +
                                    "<i class='inner i-cat-" + data.category_main + "'></i>",
                }) );
                leafletMarker.on('click', function(e){
                    $http.get('/api/active?id=' + data.id, {
                        ignoreLoadingBar: true,
                    }).then( function(response){
                        $rootScope.$broadcast('open-marker', response.data);
                    });
                });
            };
        }
        return clusters;
    };

    // Fetches data from API and caches it in service data
    // Returns data in the given format
    actives.setup = function(format){
        this.createCategories();
        if( actives_data.get('actives') == null ) {
            return $http.get('/api/actives', { cache: true }).then( function(response){
                actives_data.put('actives', JSON.parse(response.data));
                if(format == 'map' ){
                    return actives.createClusters();
                } else {
                    return actives.createList();
                }
            }, function(error_response){
                console.log(error_response);
            });
        } else {
            return $q( function(resolve, reject){
                if(format == 'map' ){
                    resolve( actives.createClusters() );
                } else {
                    resolve( actives.createList() );
                }
            }).then( function(clusters) {
                return clusters;
            });
        }
    };

    // Get relations tree of a given initiatiave
    actives.getRelations = function(id){
        var items = actives_data.get('actives');
        var marker = items.filter(i=>i.pk==id)[0];
        var markers = [];
        var visited_ids = [];
        var relations = [];
        while(marker){
            visited_ids.push(marker.pk);
            var marker_relations = marker.actives;
            marker_relations.forEach(id=>{
                var related_marker = items.filter(i=>i.pk==id)[0];
                if( visited_ids.indexOf(id) == -1 ){
                    markers.push(related_marker);
                    relations.push([
                      JSON.parse(marker.position).coordinates.reverse(),
                      JSON.parse(related_marker.position).coordinates.reverse(),
                      marker.pk
                    ]);
                }
            });
            marker = markers.shift();
        }
        return relations;
    }

    return actives;

});
