'use strict';

/**
 *  i18n
 *  A service to handle interface translation
 */
angular.module('civics.i18n', [])

.filter('t', function(Langs) {
    return function(text){
        return Langs.get(text);
    };
})

.factory('Langs', function()
{
    var interfaz = {
      eu: {
          //views/content-list.html
          'Mostrando' : 'Erakutsi',
          'elementos filtrados' : 'erakustea',
          'Ver página anterior' : 'Aurreko orria ikustea',
          'Ver página siguiente' : 'Hurrengo orria ikustea',
          //views/content-featured.html
          'Volver atrás' : 'Atzerantz',
          'Activos destacados' : 'Aktiboak nabarmena',
          'Últimos activos incorporados' : 'Sartutako azken aktiboak',
          'Parece que todavía no se han generado contenidos de este tipo' : 'Badirudi oraindik ez direla era horretako edukiak sortu',
          //directives/map-actions.html
          'Ver los activos en formato mapa' : 'Ikusi aktiboak maparen formatuan',
          'Ver los activos en formato lista' : 'Ikusi aktiboak zerrenda formatuan',
          'Ver los activos destacados' : 'Ikusi aktiboak nabarmena',
          'Mostrando' : 'Erakutai',
          'elementos de' : 'elementuren',
          'activos' : 'aktiboak',
          'Eliminar filtros' : 'Iragazkiak berrabiarazi',
          //directives/map-filters.html
          'Ciudad' : 'Hiria',
          'Categorías' : 'Kategoriak',
          'Perspectivas' : 'Ikuspegiak',
          'Quién mapea' : 'Nork mapeatzen du',
          //directives/search.html
          'Busca por' : 'Bilatu honen arabera',
          'Nombre' : 'Izena',
          'Concepto' : 'Konzeptua',
          //directives/social-widget.html
          'Perfil de Solasgune en Facebook' : 'Solasguneren profila Facebooken',
          'Perfil de Solasgune en Twitter' : 'Solasguneren profila Twitterren',
          'Repositorio del mapa de activos en Github' : 'Repositorio del mapa de activos en Github',
          'Contacta con Solasgune' : 'Solasgunekin harremanetarako',
          //services/Categories.js
          'Activos de las personas' : 'Gizabanakoen aktiboak',
          'Activos de las asociaciones y entidades' : 'Elkarteen eta erakundeen aktiboak',
          'Activos de la administración pública' : 'Administrazio publikoen aktiboak',
          'Activos físicos' : 'Aktibo fisikoak',
          'Activos económicos' : 'Aktibo ekonomikoak',
          'Activos culturales' : 'Aktibo kulturalak',
          'Equidad' : 'Ekitatea',
          'Participación' : 'Parte-hartzea',
          'Fortalecimiento comunitario' : 'Komunitarioa indartzea',
          'Otra' : 'Beste bat',
          'Instituciones públicas' : 'Erakunde publikoak',
          'Ciudadanía' : 'Herritarrak',
          'Coincidencias' : 'Kontzidentziak',
          'Diferencias' : 'Desberdinak',
          // help_texts
          'Generar un enlace para compartir la vista actual' : 'Gaurko ikuspegia partekatzeko esteka sortzea',
          'Ver contenido en formato lista' : 'Ikusi edukia zerrenda formatuan',
          'Ver contenido en formato mapa' : 'Ikusi edukia mapa formatuan',
          'Ver contenido destacado' : 'Ikusi edukia nabarmena',
          'Descargar la vista' : 'Ikuspegia deskargatu',
          'Descargar vista actual en formato XLS o CSV' : 'Deskargatu gaurko ikuspegia XLS edo CSV formatuan',
          'Descargar la vista en XLS' : 'Deskargatu gaurko ikuspegia XLS formatuan',
          'Descargar la vista en CSV' : 'Deskargatu gaurko ikuspegia CSV formatuan',
          'Puedes usar este enlace para compartir la vista actual del mapa' : 'Esteka hau erabil dezakezu maparen egungo ikuspegia partekatzeko',
          'Generar un enlace para compartir la vista actual' : 'Gaurko ikuspegia partekatzeko esteka sortzea',
          'Cambiar la capa base del mapa' : 'Maparen oinarrizko geruza aldatzea',
          'Volver a la vista inicial de todo el contenido' : 'Eduki osoaren hasierako ikuspegira itzultzea',
          'Localízate en el mapa' : 'Kokatu mapan',
          'En esta esquina tienes distintas acciones que puedes realizar sobre el contenido mostrado:' : 'Izkina honetan, erakutsitako edukiaren gainean egin ditzakezun hainbat ekintza dituzu:',
          'Ver contenido en formato mapa' : 'Ikusi edukia zerrenda formatuan',
          'Puedes usar este buscador para encontrar activos por nombre (busca coincidencias sólo en los nombres de los activos) o por concepto (busca coincidencias tanto en los nombres como en las descripciones de los activos). Se autocompleta a partir de 3 caracteres insertados' : 'Bilatzailea erabil dezakezu izenaren araberako aktiboak (kointzidentziak aktiboen izenean bakarrik bilatzen ditu) edo kontzeptuka aurkitzeko (kointzidentziak bilatzen ditu, bai izenetan, bai aktiboen deskribapenetan). Hiru karaktere txertatu ondoren osatzen da bilaketa.',
          'Usa estos botones para encontrarnos en otras plataformas o contactar con nosotros' : 'Erabili botoiak beste plataforma batzuetan topatzeko edo gurekin harremanetan jartzeko',
          'Esta es la leyenda del mapa. Puedes pulsar en las ciudades y distintas categorías para filtrar los contenidos visibles según tus intereses' : 'Hau da maparen legenda. Hirietan eta kategorietan sakatu egin dezakezu zure interesen arabera ikus daitezkeen edukiak iragazteko',
          // Other
          'Promueve' : 'Sustatzen',
          'Saber más sobre' : 'Gehiago jakin',
          'Activos relacionados' : 'Lotutako aktiboak',
          'Los activos relacionados están conectados por líneas discontinuas con el activo seleccionado' : 'Los activos relacionados están conectados por líneas discontinuas con el activo seleccionado',
        },
    };

    var l = window.location.pathname.split('/')[1];

    return {
        lang : l,

        langs : [
          { k: 'eu', v: 'eu'},
          { k: 'es', v: 'es'},
        ],

        get: function(text){
            var lang = this.lang;
            return (lang == 'e' || !interfaz[lang] ) ? text : interfaz[lang][text];
        }
    }
});
