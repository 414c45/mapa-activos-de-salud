# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-20 18:09+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps/api/views.py:25
msgid ""
"No se han encontrado resultados que cumplan con todas las condiciones de "
"filtrado."
msgstr ""

#: apps/api/views.py:195
msgid "Nombre de la iniciativa"
msgstr ""

#: apps/api/views.py:196
msgid "Descripción"
msgstr ""

#: apps/api/views.py:197
msgid "Temática"
msgstr ""

#: apps/api/views.py:198
msgid "Perspectiva"
msgstr ""

#: apps/api/views.py:199 apps/models/models.py:186
msgid "Agente"
msgstr ""

#: apps/api/views.py:200
msgid "Web"
msgstr ""

#: apps/api/views.py:201 apps/models/forms.py:101 apps/models/models.py:80
#: apps/models/models.py:195
msgid "Ciudad"
msgstr ""

#: apps/api/views.py:202
msgid "Latitud"
msgstr ""

#: apps/api/views.py:203
msgid "Longitud"
msgstr ""

#: apps/api/views.py:204
msgid "Fecha de creación"
msgstr ""

#: apps/models/categories.py:8
msgid "Activos de las personas"
msgstr ""

#: apps/models/categories.py:9
msgid "Activos de las asociaciones y entidades"
msgstr ""

#: apps/models/categories.py:10
msgid "Activos de la administración pública"
msgstr ""

#: apps/models/categories.py:11
msgid "Activos físicos"
msgstr ""

#: apps/models/categories.py:12
msgid "Activos económicos"
msgstr ""

#: apps/models/categories.py:13
msgid "Activos culturales"
msgstr ""

#: apps/models/categories.py:17
msgid "Equidad"
msgstr ""

#: apps/models/categories.py:18
msgid "Participación"
msgstr ""

#: apps/models/categories.py:19
msgid "Fortalecimiento comunitario"
msgstr ""

#: apps/models/categories.py:20
msgid "Otra"
msgstr ""

#: apps/models/categories.py:24
msgid "Instituciones públicas"
msgstr ""

#: apps/models/categories.py:25
msgid "Ciudadanía"
msgstr ""

#: apps/models/forms.py:29 apps/models/forms.py:124
msgid "Localiza la dirección"
msgstr ""

#: apps/models/forms.py:39
msgid "Ya existe una ciudad con ese nombre"
msgstr ""

#: apps/models/forms.py:47 apps/models/models.py:226
msgid "Activos relacionados"
msgstr ""

#: apps/models/forms.py:49
msgid ""
"Escoge de la columna izquierda los activos relacionados y selecciónalos "
"pasándolos a la columna derecha pulsando el botón \"Elegir\". Puedes usar el "
"filtro de la columna izquierda para buscar los activos por su nombre. Ten en "
"cuenta que éste es sensible a mayúsculas, minúsculas y signos de puntuación. "
"Puedes hacer selecciones múltiples con la tecla Ctrl pulsada (Command en MAC)"
msgstr ""

#: apps/models/forms.py:58
msgid "Elementos"
msgstr ""

#: apps/models/forms.py:80
msgid ""
"No puedes añadir tu iniciativa a este campo. Seleccionala en la columna "
"derecha y usa el botón \"Eliminar\" para quitarla de la selección."
msgstr ""

#: apps/models/forms.py:103 apps/models/models.py:200
msgid ""
"Ciudad donde se encuentra el activo. Si no encuentras la ciudad en el "
"desplegable usa el botón inferior para añadirla."
msgstr ""

#: apps/models/forms.py:111
msgid "Añade una ciudad"
msgstr ""

#: apps/models/models.py:42
msgid "Nombre de la ciudad [EU]"
msgstr ""

#: apps/models/models.py:47
msgid "Especifica el nombre de la ciudad en euskera"
msgstr ""

#: apps/models/models.py:51
msgid "Nombre de la ciudad [ES]"
msgstr ""

#: apps/models/models.py:56
msgid "Especifica el nombre español de la ciudad"
msgstr ""

#: apps/models/models.py:59
msgid "País"
msgstr ""

#: apps/models/models.py:61
msgid "¿A qué país pertenece la ciudad?"
msgstr ""

#: apps/models/models.py:66 apps/models/models.py:215
msgid "Ubicación"
msgstr ""

#: apps/models/models.py:69
msgid "Añade la ubicación de la ciudad."
msgstr ""

#: apps/models/models.py:81
msgid "Ciudades"
msgstr ""

#: apps/models/models.py:96
msgid "Nombre del activo"
msgstr ""

#: apps/models/models.py:100
msgid "Especifica el nombre del activo"
msgstr ""

#: apps/models/models.py:111
msgid "Destacado"
msgstr ""

#: apps/models/models.py:114
msgid "Indica si es un activo destacado"
msgstr ""

#: apps/models/models.py:118
msgid "Gestora o gestor"
msgstr ""

#: apps/models/models.py:124 apps/models/models.py:269
msgid "Imagen"
msgstr ""

#: apps/models/models.py:132
msgid ""
"Sube una imagen representativa del activo haciendo click en la imagen "
"inferior. La imagen ha de tener ancho mínimo de 600 píxeles y máximo de "
"1920, y altura mínima de 300 píxeles y máxima de 1280. Formatos permitidos: "
"PNG, JPG, JPEG."
msgstr ""

#: apps/models/models.py:148
msgid "Descripción del activo"
msgstr ""

#: apps/models/models.py:152
msgid ""
"Describe el activo. ¿Qué crees que aporta este activo a tu bienestar y\\/o "
"al bienestar de la comunidad?"
msgstr ""

#: apps/models/models.py:157
msgid "Website"
msgstr ""

#: apps/models/models.py:160
msgid "Puedes enlazar una web que ayude a conocer mejor el activo."
msgstr ""

#: apps/models/models.py:163
msgid "Perspectiva principal"
msgstr ""

#: apps/models/models.py:168
msgid "Perspectiva principal del activo. Definirá el icono del activo"
msgstr ""

#: apps/models/models.py:171
msgid "Otras perspectivas"
msgstr ""

#: apps/models/models.py:175
msgid "Otras perspectivas del activo. No hace falta que repitas la principal"
msgstr ""

#: apps/models/models.py:178 apps/models/models.py:183
msgid "Categoría del activo"
msgstr ""

#: apps/models/models.py:191
msgid "Agente que mapea el activo"
msgstr ""

#: apps/models/models.py:205
msgid "Dirección"
msgstr ""

#: apps/models/models.py:210
msgid ""
"Dirección del activo. No es necesario que vuelvas a introducir la ciudad del "
"activo."
msgstr ""

#: apps/models/models.py:219
msgid ""
"Tras añadir ciudad y dirección puedes ubicar el activo pulsando el botón "
"inferior y ajustando la posición del marcador posteriormente."
msgstr ""

#: apps/models/models.py:231
msgid "Activo"
msgstr ""

#: apps/models/models.py:232 templates/regions/toolbar.html:20
msgid "Activos"
msgstr "Activos"

#: apps/models/models.py:257
msgid "Nombre del recurso"
msgstr ""

#: apps/models/models.py:263
msgid "Nombre del recurso en español"
msgstr ""

#: apps/models/models.py:275
msgid "Archivo"
msgstr ""

#: apps/models/models.py:280
msgid "Enlace"
msgstr ""

#: apps/models/models.py:282
msgid "Si el recurso es un site externo puedes indicar la url aquí."
msgstr ""

#: apps/models/models.py:285
msgid "Descripción corta del recurso"
msgstr ""

#: apps/models/models.py:290
msgid "Descripción corta del recurso [español]"
msgstr ""

#: apps/models/models.py:296
#, fuzzy
#| msgid "Recursos"
msgid "Recurso"
msgstr "Recursos"

#: apps/models/models.py:297 templates/pages/resources.html:10
#: templates/pages/resources.html:19 templates/regions/toolbar.html:25
msgid "Recursos"
msgstr "Recursos"

#: apps/models/validators.py:15
#, python-format
msgid "El ancho de la imagen debería ser mayor a %(min_width)d píxeles"
msgstr ""

#: apps/models/validators.py:16
#, python-format
msgid "El ancho de la imagen debería ser menor a %(max_width)d píxeles"
msgstr ""

#: apps/models/validators.py:17
#, python-format
msgid "El alto de la imagen debería ser mayor a %(min_height)d píxeles"
msgstr ""

#: apps/models/validators.py:18
#, python-format
msgid "El alto de la imagen debería ser menor a %(max_height)d píxeles"
msgstr ""

#: apps/models/views.py:33
msgid "Crea un activo [1/2]"
msgstr ""

#: apps/models/views.py:47
msgid "Crea el activo y añade relaciones con otros activos"
msgstr ""

#: apps/models/views.py:57
msgid "Edita la información del activo"
msgstr ""

#: apps/models/views.py:79 apps/models/views.py:103
msgid "Guarda los cambios"
msgstr ""

#: apps/models/views.py:88
msgid "Relaciona tu activo"
msgstr ""

#: apps/models/views.py:113
msgid "Borra la iniciativa "
msgstr ""

#: apps/models/views.py:123
msgid "¿Estás seguro de que quieres borrar este activo?"
msgstr ""

#: apps/models/views.py:124
msgid "No, no estoy seguro."
msgstr ""

#: apps/users/views.py:71
msgid "Tu cuenta ha sido cancelada"
msgstr ""

#: apps/users/views.py:75
msgid "Tu cuenta eha sido cancelada"
msgstr ""

#: apps/users/views.py:76
msgid ""
"Te confirmamos que la cuenta asociada a este correo ha sido borrada con "
"éxito. Este es un correo automático. No lo respondas."
msgstr ""

#: apps/utils/templates/fake-breadcrumb.html:5
msgid "Volver a la página anterior"
msgstr ""

#: apps/utils/templates/limited-choices-select.html:5
msgid "Todas las opciones"
msgstr ""

#: apps/utils/templates/limited-textarea-widget.html:6
msgid "Has escrito"
msgstr ""

#: apps/utils/templates/limited-textarea-widget.html:6
msgid "caracteres. Tienes un límite de "
msgstr ""

#: apps/utils/templates/share.html:4
msgid "Comparte"
msgstr ""

#: apps/utils/views.py:30
msgid "Contenido creado con éxito"
msgstr ""

#: apps/utils/views.py:48
msgid "Contenido editado con éxito"
msgstr ""

#: apps/utils/views.py:70
msgid "Contenido borrado con éxito"
msgstr ""

#: civics/settings.py:122
msgid "Euskera"
msgstr ""

#: civics/settings.py:123
msgid "Español"
msgstr ""

#: templates/403.html:12
msgid "No tienes permiso para acceder a esta página"
msgstr ""

#: templates/404.html:12
msgid "La página que buscas no existe. ¿Las has escrito correctamente?"
msgstr ""

#: templates/503.html:9
msgid "En mantenimiento"
msgstr ""

#: templates/503.html:29
msgid "Estamos en mantenimiento, perdona las molestias. Volvemos en un rato!"
msgstr ""

#: templates/503.html:35 templates/pages/goodbay.html:17
msgid "Recuerda que puedes seguirnos y contactarnos en:"
msgstr ""

#: templates/503.html:40
msgid "Perfil de Civics en Facebook"
msgstr ""

#: templates/503.html:45
msgid "Perfil de Civics en Twitter"
msgstr ""

#: templates/base.html:11
msgid "Mapa de activos de salud"
msgstr ""

#: templates/blocks/cookie-consent.html:8
msgid ""
"Este sitio usa <a href='//matomo.org' target=\"_blank\">Matomo</a> para "
"analizar el tráfico y ayudarnos a mejorar la experiencia de usuario. "
"Procesamos tu dirección IP en una cookie que permanece en tu navegador por "
"13 meses. Estos datos sólo son procesados por nosotros y en ningún caso se "
"comparten con terceros. Lee nuestra <a href=\"\">Política de Privacidad</a> "
"si quieres saber más."
msgstr ""

#: templates/blocks/cookie-consent.html:19
msgid "Aceptar y continuar"
msgstr ""

#: templates/blocks/popup-ods.html:11
msgid ""
"Ya puedes vincular tu iniciativa ciudadana a los <strong>Objetivos de "
"Desarrollo Sostenible (ODS)</strong> de la Agenda 2030. Un acuerdo entre "
"Estados que contó con la participación de la sociedad civil y que está "
"basado en 17 Objetivos relacionados con la reducción de la desigualdad, el "
"cuidado del medio ambiente o el distrute de derechos sin discriminación "
"alguna. Vincula el tuyo!!"
msgstr ""

#: templates/contact_form/contact_form.html:11
#: templates/forms/form-relations.html:28 templates/forms/form.html:41
#: templates/pages/about.html:16 templates/pages/resources.html:16
#: templates/users/user_confirm_delete.html:21
msgid "Volver atrás"
msgstr ""

#: templates/contact_form/contact_form.html:14
msgid "Contacta con nosotras"
msgstr ""

#: templates/contact_form/contact_form.txt:3
#, python-format
msgid ""
"\n"
"Hay un nuevo mensaje de contacto en la plataforma CIVICS.CC.\n"
"\n"
"Mensaje de %(name)s [%(email)s]:\n"
"\"%(body)s\"\n"
"\n"
msgstr ""

#: templates/contact_form/contact_form_sent.html:7
msgid "Gracias!"
msgstr ""

#: templates/contact_form/contact_form_sent.html:8
msgid ""
"Gracias por contactar con nosotr-s. Te hemos enviado un correo de "
"confirmación. ¡Gracias por el interés!"
msgstr ""

#: templates/contact_form/contact_form_sent.html:13
msgid "Volver"
msgstr ""

#: templates/contact_form/contact_form_subject.txt:2
msgid ""
"\n"
"Mensaje nuevo de contacto en civics.cc\n"
msgstr ""

#: templates/forms/city-popup-form.html:10
#: templates/forms/form-relations.html:6 templates/forms/form.html:6
#: templates/forms/regular-form.html:6
msgid "Parece que hay un error en el formulario. Revísalo, por favor."
msgstr ""

#: templates/forms/city-popup-form.html:12
#: templates/forms/form-relations.html:8 templates/forms/form.html:8
#: templates/forms/regular-form.html:8
msgid "Parece que hay errores en el formulario. Revísalo, por favor.."
msgstr ""

#: templates/forms/city-popup-form.html:31
msgid "Envía"
msgstr ""

#: templates/forms/form-relations.html:31
msgid "Cancelar e ir al mapa"
msgstr ""

#: templates/forms/form.html:29
msgid "Edita las relaciones"
msgstr ""

#: templates/forms/form.html:33
msgid "Borra este contenido"
msgstr ""

#: templates/forms/form.html:39
msgid "Cancelar"
msgstr "Cancelar"

#: templates/forms/form.html:46
msgid "Cancelar e ir a tu perfil"
msgstr ""

#: templates/pages/about.html:10 templates/regions/toolbar.html:30
msgid "Acerca"
msgstr "Acerca"

#: templates/pages/about.html:22
msgid "ABOUT_SUBTITLE"
msgstr "MAPEANDO EL BIENESTAR DE LA COMUNIDAD"

#: templates/pages/about.html:25
msgid "ABOUT_FIRST"
msgstr ""
"<p>Vivimos en sociedades donde un reducido número de personas deciden, en parte, "
"de qué manera organizar nuestras vidas. Este modelo social nos ha hecho "
"dependientes de unas formas y maneras de hacer que limitan nuestra autonomía "
"y nuestro poder y capacidad de organización, acción y decisión. Todo ello conlleva "
"que, las capacidades y las fortalezas de la ciudadanía, queden ocultas e invisibles y, "
"por tanto, perdamos la oportunidad de crear juntas, de hacer juntas, en definitiva, "
"de hacer y trabajar en comunidad.</p>"
"<p>Por otro lado, cuando hablamos de salud, la entendemos como ausencia de patología. "
"Pero la salud es mucho más que eso: es sentirnos bien, es estar bien, es bienestar, "
"no solo personal, sino también, comunitario: porque somos comunidad y vivimos en comunidad.</p>"

#: templates/pages/about.html:26
msgid "ABOUT_SECOND"
msgstr ""
"<p>Comentábamos al principio de estas líneas que las personas tenemos capacidades y "
"fortalezas, inquietudes, necesidades y deseos que, hasta la fecha, han sido "
"invisibilizados como recurso y potencial comunitario.<p>"
"<p>Cuando pensamos en una vía verde por la que pasear, disfrutar del entorno y "
"compartir espacios con otras personas, ¿pensamos en lo que la caminata está aportando "
"a nuestro bienestar, a nuestra salud y a nuestra comunidad? Este es uno de los "
"objetivos de la herramienta que presentamos, pensar en todo aquello que nos aporta "
"bienestar, en todo aquello que nos hace sentir bien. Pero, sobre todo, identificarlo, "
"ponerlo en valor y hacerlo público. La idea es que entre todas las personas podamos "
"crear un mapa en el que identifiquemos todo aquello que mejora nuestro bienestar: "
"el espacio público que nos ofrece nuestro ayuntamiento para hacer deporte, "
"la asociación cultural de nuestro barrio que organiza lecturas al aire libre o esa "
"persona que cada mañana nos saluda sonriente cuando vamos a comprarle el pan.</p>"

#: templates/pages/about.html:30
#, python-format
msgid ""
"<h5>Política de privacidad</h5> <p>Puedes leer nuestra política de "
"privacidad en la página enlazada a continuación:</p> <a href="
"\"%(contact_url)s\">Política de privacidad</a>"
msgstr ""

#: templates/pages/about.html:39
msgid "FAQ"
msgstr "Preguntas frecuentes"

#: templates/pages/about.html:58
msgid ""
"Esta obra está bajo <a href=\"https://creativecommons.org/licenses/by-sa/4.0/"
"\" target=\"_blank\"> Licencia Creative Commons Atribución-CompartirIgual "
"4.0 internacional</a> Los datos recopilados están bajo <a href=\"https://"
"opendatacommons.org/licenses/odbl/1.0/\" target=\"_blank\"> licencia ODbL "
"1.0.</a> El código fuente de la plataforma está disponible bajo una licencia "
"<a href=\"https://www.gnu.org/licenses/gpl.html\" target=\"_blank\"> General "
"Public License 3.0</a>."
msgstr ""

#: templates/pages/front.html:12
msgid "El mapa"
msgstr ""

#: templates/pages/goodbay.html:6
msgid "Logout"
msgstr ""

#: templates/pages/goodbay.html:12
msgid "Gracias por visitarnos."
msgstr ""

#: templates/pages/goodbay.html:22
msgid "Perfil de Solasgune en Facebook"
msgstr ""

#: templates/pages/goodbay.html:27
msgid "Perfil de Solasgune en Twitter"
msgstr ""

#: templates/pages/goodbay.html:32
msgid "Contacta con Solasgune"
msgstr ""

#: templates/pages/initiative-success.html:7
msgid "¡Enhorabuena, tu iniciativa ha sido incorporada!"
msgstr ""

#: templates/pages/initiative-success.html:10
msgid "Ve al perfil de tu iniciativa"
msgstr ""

#: templates/pages/modelform.html:15
msgid "Crea un activo"
msgstr "Crea un activo"

#: templates/pages/privacy.html:11
msgid "Política de privacidad de civics.cc"
msgstr ""

#: templates/pages/privacy.html:15
msgid ""
"<p> Civics (\"nosotros\", \"nosotros\" o \"nuestro\") opera el sitio web "
"civics.cc (el \"Servicio\"). <br/> Esta página le informa de nuestras "
"políticas con respecto a la recopilación, uso y divulgación de Información "
"Personal cuando usa nuestro Servicio.<br/> No utilizaremos ni compartiremos "
"su información con nadie, excepto como se describe en esta Política de "
"privacidad.<br/> Utilizamos su información Personal para proporcionar y "
"mejorar el Servicio. Al utilizar el Servicio, usted acepta la recopilación y "
"el uso de la información de acuerdo con esta política. A menos que se defina "
"lo contrario en esta Política de Privacidad, los términos usado en esta "
"Política de Privacidad tienen los mismos significados que en nuestros "
"Términos y Condiciones, accesibles en civics.cc. </p> <h3 class=\"privacy-"
"policy__subtitle\"> Recopilación y uso de información </h3> <p> Durante el "
"uso de nuestro Servicio, podemos pedirle que nos proporcione cierta "
"información de identificación personal que pueda ser utilizada para "
"contactarle o identificarle, como pueden ser correo electrónico y un nombre "
"de usuario.<br/> <h3 class=\"privacy-policy__subtitle\"> Datos de registro </"
"h3> <p> Recopilamos información que envía su navegador cada vez que visita "
"nuestro Servicio (\"Datos de Registro\"). Estos datos de registro pueden "
"incluir información como la dirección de IP de su computadora, el tipo de "
"navegador, la versión del navegador, las páginas de nuestro servicio que "
"visita, la hora y la fecha de su visita, el tiempo dedicado a esas páginas y "
"otas estadísticas. </p> <h3 class=\"privacy-policy__subtitle\"> Cookies </"
"h3> <p> Las cookies son archivos con poca cantidad de datos, que pueden "
"incluir un identificador único anónimo. Las cookies se envían a su navegador "
"desde un sitio web y se almacenan en el disco duro de su computadora. </"
"p><br/> <p>En este sitio usamos las siguientes cookies:</p> <br/> <ul> "
"<li><em>Cookies de Sesión</em>. Las Cookies de Sesión son aquellas "
"destinadas a mantener abierta la sesión de los usuarios registrados y se "
"usan con fines puramente técnicos.</li> <li><em>Cookies de Análisis</em>. "
"Son aquéllas que bien tratadas por nosotros nos permiten cuantificar el "
"número de usuarios y así realizar la medición y análisis estadístico de la "
"utilización que hacen los usuarios del servicio ofertado. Para ello se "
"analiza su navegación en nuestra página web con el fin de mejorar los "
"servicios que te ofrecemos.</li> </ul> <h3 class=\"privacy-policy__subtitle"
"\"> Enlaces a otros sitios </h3> <p> Nuestro Servicio puede contener enlaces "
"a otros sitios que no son operados por nosotros. Si hace clic en un enlace "
"de terceros, se le dirigirá a sitio de ese tercero. Le recomendamos que "
"revise la Política de privacidad de cada sitio que visita. <br/> No tenemos "
"ningún control sobre, ni asumimos ninguna responsabilidad por el contenido, "
"políticas de privacidad o prácticas de sitios o servicios de terceros. </p> "
"<h3 class=\"privacy-policy__subtitle\"> Privacidad de los niños </h3> <p> "
"Nuestro Servicio no se dirige a menores de 13 años (\"Niños\"). No "
"recopilamos a sabiendas información de identificación personal de niños "
"menores de 13 años. Si usted es un padre o tutor y usted es consciente de "
"que sus hijos nos han proporcionado información personal, por favor "
"contáctenos. Si descubrimos que un Menor de 13 años nos ha proporcionado "
"Información Personal, eliminaremos dicha información de nuestros servidores "
"inmediatamente. </p> <h3 class=\"privacy-policy__subtitle\"> Cambios a esta "
"política de privacidad </h3> <p> Podemos actualizar nuestra Política de "
"privacidad de vez en cuando. Le notificaremos de cualquier cambio publicando "
"la nueva Política de privacidad en esta página. <br/> Se recomienda revisar "
"esta Política de privacidad periódicamente para cualquier cambio. Los "
"cambios a esta Política de privacidad son efectivos cuando se publican en "
"esta página. </p> <h3 class=\"privacy-policy__subtitle\"> Contáctenos </h3> "
"<p> Si tiene alguna pregunta sobre esta Política de Privacidad, <a href=\"/"
"contacta\"> póngase en contacto con nosotros.</a> </p>"
msgstr ""

#: templates/pages/resources.html:27
msgid "Edita"
msgstr "Edita"

#: templates/regions/toolbar.html:6
msgid "Ir a la pantalla de inicio"
msgstr ""

#: templates/regions/toolbar.html:40
msgid "Añade un activo"
msgstr "Añade un activo"

#: templates/regions/toolbar.html:46
msgid "Añade un recurso"
msgstr "Añade un recurso"

#: templates/regions/toolbar.html:54
msgid "Administra"
msgstr "Administra"

#: templates/regions/toolbar.html:62
msgid "Sal de la plataforma"
msgstr "Sal de la plataforma"

#: templates/regions/toolbar.html:67
msgid "Entra en la plataforma o regístrate en ella"
msgstr ""

#: templates/registration/activation_complete.html:9
msgid "Bienvenid@"
msgstr ""

#: templates/registration/activation_complete.html:14
msgid "Has activado la cuenta."
msgstr ""

#: templates/registration/activation_email.html:2
#, python-format
msgid ""
"\n"
"    Alguien ha registrado un usuario/a en la plataforma civics.cc usando "
"este correo.\n"
"    Si eres tú, visita este enlace para activar la cuenta haciendo login con "
"los datos\n"
"    que has registrado:\n"
"        http://%(site)s/activate/%(activation_key)s\n"
"\n"
msgstr ""

#: templates/registration/activation_email.html:9
#, python-format
msgid ""
"\n"
"  Este enlace es válido por %(expiration_days)s días.\n"
msgstr ""

#: templates/registration/login.html:6 templates/registration/login.html:18
msgid "Login"
msgstr ""

#: templates/registration/login.html:13
#: templates/registration/password_reset_complete.html:9
#: templates/registration/password_reset_confirm.html:9
#: templates/registration/password_reset_form.html:12
#: templates/registration/registration_form.html:13
msgid "Ya estás autenticado en la plataforma."
msgstr ""

#: templates/registration/login.html:20
msgid "Accede"
msgstr ""

#: templates/registration/login.html:26
msgid "¿Has olvidado tu nombre de usuari@ o contraseña?"
msgstr ""

#: templates/registration/password_reset_complete.html:19
#, python-format
msgid ""
"La contraseña se ha cambiado con éxito. Ahora puedes <a href=\"%(login_url)s"
"\">hacer login</a> en la plataforma."
msgstr ""

#: templates/registration/password_reset_confirm.html:18
msgid ""
"Introduce la nueva contraseña. Hazlo por duplicado para comprobar que la has "
"escrito bien."
msgstr ""

#: templates/registration/password_reset_done.html:8
msgid ""
"Te hemos enviado instrucciones para cambiar tu contraseña a tu correo "
"electrónico. <br/> <em>Si no encuentras el correo comprueba el correo "
"electrónico que has introducido se corresponde al de un usuari@ y no se te "
"olvide comprobar la bandeja de spam</em>."
msgstr ""

#: templates/registration/password_reset_email.html:4
#, python-format
msgid ""
"\n"
"Has recibido este correo porque has requerido un cambio de contraseña para "
"tu cuenta de usuari@ en %(site_name)s.\n"
msgstr ""

#: templates/registration/password_reset_email.html:8
msgid "Usa el siguiente enlace para escoger una contraseña nueva:"
msgstr ""

#: templates/registration/password_reset_email.html:14
msgid "Tu nombre de usuari@ en caso de que lo hayas olvidado es:"
msgstr ""

#: templates/registration/password_reset_email.html:17
msgid "Gracias por ser parte de civics.cc"
msgstr ""

#: templates/registration/password_reset_form.html:5
msgid "Olvidaste la contraseña?"
msgstr ""

#: templates/registration/password_reset_form.html:21
msgid ""
"Si has olvidado tu contraseña introduce el correo electrónico asociado a la "
"cuenta y recibirás en el mismo instrucciones para cambiarla."
msgstr ""

#: templates/registration/registration_complete.html:8
msgid ""
"Has completado el registro con éxito. Hemos enviado a tu buzón de correo "
"electrónico un correo con un enlace para activar tu cuenta. Vísitalo para "
"activar la cuenta y poder registrar tu iniciativa en la plataforma. <br/"
"><em>Si no encuentras el correo, no se te olvide comprobar la bandeja de "
"spam</em>."
msgstr ""

#: templates/registration/registration_form.html:18
msgid "Regístrate"
msgstr ""

#: templates/registration/registration_form.html:20
msgid "Crea un usuari@"
msgstr ""

#: templates/users/dashboard.html:12
msgid "Mi perfil"
msgstr ""

#: templates/users/dashboard.html:14
msgid "Cancelar mi usuario"
msgstr ""

#: templates/users/dashboard.html:21
msgid "Mis iniciativas"
msgstr ""

#: templates/users/dashboard.html:23
msgid "Crea una iniciativa"
msgstr ""

#: templates/users/dashboard.html:44
msgid "Fecha de alta:"
msgstr ""

#: templates/users/dashboard.html:48
msgid "Editar"
msgstr ""

#: templates/users/dashboard.html:59
msgid "Mapa de iniciativas"
msgstr ""

#: templates/users/user_confirm_delete.html:8
msgid "Signout"
msgstr ""

#: templates/users/user_confirm_delete.html:12
msgid ""
"¿Estás seguro de que quieres cancelar tu cuenta? Esta acción no se puede "
"deshacer y borrará a tu usuario y a todo el contenido asociado al mismo."
msgstr ""

#: templates/users/user_confirm_delete.html:19
msgid "Cancela mi cuenta"
msgstr ""

# FAQ

msgid "faq_what-is-active_label"
msgstr "¿Qué es un activo en salud?"

msgid "faq_what-is-active_text"
msgstr ""
"Activos en salud son las riquezas, factores o recursos que preservan, "
"potencian o mejoran la salud o el bienestar de las personas. Son el patrimonio "
"de las personas y de los barrios y comunidades para mantener la salud."

msgid "faq_differences_label"
msgstr "¿Diferencia entre activos en salud y recurso?"

msgid "faq_differences_text"
msgstr ""
"La diferencia fundamental es que los recursos vienen dados por el territorio y "
"los activos son identificados y puestos en valor por la comunidad."

msgid "faq_why_label"
msgstr "¿Por qué trabajar con activos en salud?"

msgid "faq_why_text"
msgstr ""
"Porque potencia factores de protección y promoción de la salud en la ciudadanía, "
"más allá de la prevención de la enfermedad. Existe evidencia de que, los "
"factores que más influyen en la salud, son los sociales, frente a factores "
"genéticos o biológicos que, además, no son modificables."

msgid "faq_what-is-map_label"
msgstr "¿Qué es mapeo de activos?"

msgid "faq_what-is-map_text"
msgstr ""
"Proceso en el que se recoge, con y para la comunidad, aquellos activos cuyo "
"valor se reconoce por lo que contribuyen a la salud de todas las personas de "
"la comunidad. Son recursos, personas, experiencias o conocimientos que tenemos "
"en nuestro entorno cercano y utilizamos (o creemos que podríamos utilizar). "
"La fortaleza del mapeo está en el proceso, ya que pretende generar equipo, red, "
"participación de la vecindad, apropiación, reconocimiento..."

msgid "faq_who_label"
msgstr "¿Quiénes mapean activos?"

msgid "faq_who_text"
msgstr ""
"Todas las personas pueden mapear. Los diferentes agentes de la comunidad pueden "
"ser: la ciudadanía, equipos profesionales y técnicos, entidades, asociaciones…"

msgid "faq_benefits_label"
msgstr "¿Qué aporta un mapeo?"

msgid "faq_benefits_text"
msgstr ""
"Identifica activos en salud de una comunidad para generar conexiones, "
"fortalecer a la comunidad, mejorar el bienestar de las personas y el conocimiento "
"del territorio. Pone a nuestro alcance el conocimiento de esos activos y, "
"por lo tanto, la posibilidad de conectarnos con ellos."

msgid "faq_objectives_label"
msgstr "¿Qué objetivos tiene esta página web?"

msgid "faq_objectives_text"
msgstr ""
"Hacer accesible y ofrecer a la población los activos locales, identificados "
"y reconocidos por todas, que pueden contribuir a mejorar nuestra salud; y"
"estudiar la distribución de los recursos por áreas/zonas e identificar "
"focos de acción para un posterior desarrollo o potenciación."
