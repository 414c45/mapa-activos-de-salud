angular.module('civics.categories_service', [])

.factory('Categories', function()
{

    this.categories = {
        '1' : 'Ecosocial',
        '2' : 'Psicosocial',
        '3' : 'Socioemocional',
        '4' : 'Diversidad en equidad',  
    };

    this.ods = { 
        '1'  : 'Fin de la pobreza',
        '2'  : 'Hambre cero',
        '3'  : 'Salud y bienestar',
        '4'  : 'Educación de calidad',
        '5'  : 'Igualdad de género',
        '6'  : 'Agua limpia y saneamiento',
        '7'  : 'Energía asequible y no contaminante',
        '8'  : 'Trabajo decente y crecimiento económico',
        '9'  : 'Industria, innovación e infraestructura',
        '10' : 'Reducción de las desigualdades',
        '11' : 'Ciudades y comunidades sostenibles',
        '12' : 'Producción y consumo responsables',
        '13' : 'Acción por el clima',
        '14' : 'Vida submarina',
        '15' : 'Vida de ecosistemas terrestres',
        '16' : 'Paz, justicia e instituciones sólidas',
        '17' : 'Alianzas para lograr los objetivos',
    };  

    this.agents = {
        '1' : 'Pública',
        '2' : 'Privada',
        '3' : 'Pública y privada',
        '4' : 'Comunitaria'
    };

    this.cities = {};

    this.addCity = function(country, name, id, coords){
        if(!this.cities[country])
            this.cities[country] = {}
        if(!this.cities[country][id]) {
            this.cities[country][id] = {
                'name'   : name,
                'coords' : [ coords[1], coords[0] ],
            };
        }
    }

    return this;

})
