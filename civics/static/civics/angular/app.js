'use strict';

/**
 * @ngdoc overview
 * @name civics
 * @description
 * Bootstrap Civics angular app
 *
 * Main module of the application.
 */
angular.module('civics', [
    'ngRoute',
    'ngSanitize',
    'angular-loading-bar',
    'leaflet-directive',
    'civics.settings',
    'civics.categories_service',
    'civics.actives_service',
    'civics.featured_service',
    'civics.downloader',
    'civics.map_controller',
    'civics.list_controller',
    'civics.featured_controller',
    'civics.directives',
    'civics.i18n',
  ])

.config([
    '$routeProvider',
    'cfpLoadingBarProvider',
    '$logProvider',
    '$compileProvider',
    '$httpProvider',
    function (
        $routeProvider,
        cfpLoadingBarProvider,
        $logProvider,
        $compileProvider,
        $httpProvider
    ){

    //To avoid excessive amounts of logs coming from the events in leaflet-directive
    $logProvider.debugEnabled(false);

    //To improve performance in production
    $compileProvider.debugInfoEnabled(false);

    //Use applysync to reduce $digest calls using ajax
    $httpProvider.useApplyAsync(true);

    //Turn off spinner in angular-loading-bar
    cfpLoadingBarProvider.includeSpinner = false;


    var templates_url = '/static/civics/angular/views/'

    $routeProvider
      .when('/activos', {
          templateUrl : templates_url + 'content-map.html',
          controller  : 'MapController',
          controllerAs: 'content',
          resolve: {
               items: function(Actives) {
                   return Actives.setup('map');
               }
          }
      })
      .when('/activos-lista', {
          templateUrl : templates_url + 'content-list.html',
          controller  : 'ListController',
          controllerAs: 'content',
          resolve: {
               items: function(Actives) {
                   return Actives.setup('list');
               }
          }
      })
      .when('/activos-destacados', {
          templateUrl : templates_url + 'content-featured.html',
          controller  : 'FeaturedController',
          controllerAs: 'content',
          resolve: {
               items: function(Featured) {
                   return Featured.setup(
                      '/api/actives_featured',
                      'actives'
                    )
               }
          }
      })
      .otherwise({
        redirectTo: '/activos'
      });
}])

.value("meta", {
    'count' : 0,
    'showing' : '',
});
