from django.core.management.base import BaseCommand, CommandError
import csv, json, os.path, random
from apps.models.models import Active, City
from django.utils.text import slugify

# TODO: tests!!!

"""
A manage.py command to migrate initiatives from a CSV file
"""

class Command(BaseCommand):
    help = "Instantiate Actives models from a CSV file. Following columns are needed: \
            'Name', 'Code' (contrycode in ISO ISO 3166-2), 'Longitude' and 'Latitude' \
            The only argument is a valid path to the CSV file."

    """
    Add CSV file as an argument to the parser
    """
    def add_arguments(self, parser):
        parser.add_argument('csv_file')

    """
    Imports Active models from a given CSV file
    """
    def handle(self, *args, **options):
        if not os.path.isfile(options['csv_file']):
            raise CommandError('Has de especificar un archivo para importar los activos desde el mismo')
        with open(options['csv_file'], 'r') as actives_file:
            actives = csv.DictReader(actives_file)
            actives_objects = []
            for i in actives:
                print('Creando activo ' + i['nombre'].strip() + ' en ' + i['ciudad'])
                city = City.objects.filter(name__iexact=i['ciudad'].strip()).first()
                address = i['dirección']
                categories = list( map( lambda i : i.strip(), i['itinerario'].split(',') ) )
                coords = city.position['coordinates']
                lat = str(coords[0] + (random.random()-.5) * .001)
                lon = str(coords[1] + (random.random()-.5) * .001)
                actives_objects.append(
                    Active(
                        name         = i['nombre'].strip(),
                        slug         = slugify(i['nombre']),
                        address      = i['dirección'],
                        category     = categories[0],
                        category_alt = categories[1:],
                        agent        = i['titularidad'],
                        ods          = i['ods'],
                        description  = i['justificación'],
                        city         = city,
                        relevance    = i['relevancia'],
                        position     = json.loads('{ "type": "Point", "coordinates": [' + lat + ',' + lon +'] }')
                    )
                )
            Active.objects.bulk_create(actives_objects)
