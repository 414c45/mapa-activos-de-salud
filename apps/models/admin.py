# django
from django.contrib import admin

# contrib
from leaflet.admin import LeafletGeoAdmin
from imagekit import ImageSpec
from imagekit.admin import AdminThumbnail
from imagekit.processors import ResizeToFill
from imagekit.cachefiles import ImageCacheFile

# project
from . import models


# Active Admin Form
class ActiveAdmin(LeafletGeoAdmin):
    model = models.Active
    ordering = (
        'name',
    )
    list_display = (
        'name',
        'category',
        'ods',
        'agent',
    )
    list_filter = (
        'category',
        'ods',
        'agent',
        'city'
    )

    def get_action_choices(self, request):

        return super(ActiveAdmin, self).get_action_choices(request, [("", "Elige una acción")])

# City Admin Form
class CityAdmin(LeafletGeoAdmin):
    model = models.City
    ordering = ('name',)
    list_display = (
        'name',
        'related_actives'
    )
    
    def related_actives(self, obj):
        return models.Active.objects.filter(
            city=obj
        ).count()

    def get_action_choices(self, request):

        return super(CityAdmin, self).get_action_choices(
            request,
            [("", "Elige una acción")]
        )


# Register models in admin
admin.site.register(models.City, CityAdmin)
admin.site.register(models.Active, ActiveAdmin)