from django.utils.translation import ugettext_lazy as _

"""
The different categories used by the Models of Civics
"""

CATEGORIES = (
    ('1', _('Ecosocial')),
    ('2', _('Psicosocial')),
    ('3', _('Socioemocional')),
    ('4', _('Diversidad en equidad')),
)

ODS = ( 
    ('1', _('Fin de la pobreza')),
    ('2', _('Hambre cero')),
    ('3', _('Salud y bienestar')),
    ('4', _('Educación de calidad')),
    ('5', _('Igualdad de género')),
    ('6', _('Agua limpia y saneamiento')),
    ('7', _('Energía asequible y no contaminante')),
    ('8', _('Trabajo decente y crecimiento económico')),
    ('9', _('Industria, innovación e infraestructura')),
    ('10',_('Reducción de las desigualdades')),
    ('11',_('Ciudades y comunidades sostenibles')),
    ('12',_('Producción y consumo responsables')),
    ('13',_('Acción por el clima')),
    ('14',_('Vida submarina')),
    ('15',_('Vida de ecosistemas terrestres')),
    ('16',_('Paz, justicia e instituciones sólidas')),
    ('17',_('Alianzas para lograr los objetivos')),
)

AGENTS = (
    ('1', _('Pública')),
    ('2', _('Privada')),
    ('3', _('Pública y privada')),
    ('4', _('Comunitaria')),
)

RESOURCES = ()