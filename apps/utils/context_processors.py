from django.conf import settings

def debug_processor(request):
    """Injects debug flag into context"""

    debug    = settings.DEBUG
    debug_js = settings.DEBUG_JS

    return locals()
